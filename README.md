<div align="center">
  <img src="public/icons/icon128.png"><br /><br />
  <img src="https://i.ibb.co/QcpGsCV/New-Project.png">
  <h4 style="margin-top:0">A Free And Lightweight Browser Extension</h4>
  <p>🔥 To inspect and change styles (color, font, etc..) of any DOM element with one click..🔥</p>

  ![Contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen) ![GitHub release](https://img.shields.io/github/release/Chromo-lib/detector/all?logo=GitHub) ![](https://badgen.net/github/license/Chromo-lib/detector)

</div>

<p align="center">
  <a href="https://addons.mozilla.org/en-US/firefox/addon/detector/" rel="nofollow">
    <img src="https://i.imgur.com/kMH6r1a.png" style="max-width:100%;"></a>

  <a href="https://microsoftedge.microsoft.com/addons/detail/mejanpidfmphlmlefkamaklajbcaoebc" rel="nofollow">
    <img src="https://i.imgur.com/n49Wiu2.png" style="max-width:100%;"></a>
  <br><br>
</p>

### Installation
- **[Edge chromium](https://microsoftedge.microsoft.com/addons/detail/detector/mejanpidfmphlmlefkamaklajbcaoebc)**
- **[Firefox addons](https://addons.mozilla.org/en-US/firefox/addon/detector/)**

### Capture
![](capture2.png)


# License
MIT
